import { getElementByQuery } from '../common';
import {
    GetMarketSalesService,
    GetTaxIncludedSalesService
} from '../../usecase/market';
import { GetGrossProfitService } from '../../usecase/profit';
import { GetCostAmountService } from '../../usecase/cost';
import { aggrigateItemTable } from '../aggrigate';
import { GetDelivelySalesService } from '../../usecase/delively';
import { aggrigateDelivelyItemTable } from '../aggrigate/aggrigateDelivelyItemTable';

export class UpdateResultEvent {
    private readonly salesOutput: HTMLOutputElement;
    private readonly taxIncludedSalesOutput: HTMLOutputElement;
    private readonly delivelySalesOutput: HTMLOutputElement;
    private readonly costAmountOutput: HTMLOutputElement;
    private readonly grossProfitOutput: HTMLOutputElement;
    private readonly grossProfitRateOutput: HTMLOutputElement;

    private readonly premiumCheckbox: HTMLInputElement;
    private readonly marketSalesTBody: HTMLTableSectionElement;
    private readonly delivelyItemTBody: HTMLTableSectionElement;
    private readonly costItemTBody: HTMLTableSectionElement;

    constructor(
        resultTable: HTMLTableElement,
        premiumCheckbox: HTMLInputElement,
        marketSalesTBody: HTMLTableSectionElement,
        delivelyItemTBody: HTMLTableSectionElement,
        costItemTBody: HTMLTableSectionElement,
    ) {
        this.salesOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="salesOutput"]', resultTable
        );
        this.taxIncludedSalesOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="taxIncludedSalesOutput"]', resultTable
        );
        this.delivelySalesOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="delivelySalesOutput"]', resultTable
        );
        this.costAmountOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="costAmountOutput"]', resultTable
        );
        this.grossProfitOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="grossProfitOutput"]', resultTable
        );
        this.grossProfitRateOutput = getElementByQuery<HTMLOutputElement>(
            'output[class="grossProfitRateOutput"]', resultTable
        );

        this.premiumCheckbox = premiumCheckbox;
        this.premiumCheckbox.addEventListener('change', this.updateResult);

        this.marketSalesTBody = marketSalesTBody;
        this.delivelyItemTBody = delivelyItemTBody;
        this.costItemTBody = costItemTBody;
        const tbodyObserver = new MutationObserver(this.updateResult);
        const observerOption: MutationObserverInit = {
            subtree: true, childList: true, characterData: true
        };
        tbodyObserver.observe(this.marketSalesTBody, observerOption);
        tbodyObserver.observe(this.delivelyItemTBody, observerOption);
        tbodyObserver.observe(this.costItemTBody, observerOption);
    }

    readonly updateResult = (): void => {
        const marketSales = new GetMarketSalesService()
                            .handle(
                                ...aggrigateItemTable(this.marketSalesTBody)
                            );
        const taxIncludedSales = new GetTaxIncludedSalesService()
                                 .handle({
                                     marketSales,
                                     isPremium: this.premiumCheckbox.checked
                                 });
        const delivelySales = new GetDelivelySalesService()
                              .handle(
                                  ...aggrigateDelivelyItemTable(
                                      this.delivelyItemTBody
                                  )
                              );
        const costAmount = new GetCostAmountService()
                           .handle(...aggrigateItemTable(this.costItemTBody));
        const grossProfit = new GetGrossProfitService().handle({
            taxIncludedSales,
            delivelySales,
            costAmount
        });
        this.salesOutput.textContent = marketSales.toLocaleString();
        this.taxIncludedSalesOutput.textContent = taxIncludedSales
                                                  .toLocaleString();
        this.delivelySalesOutput.textContent = delivelySales.toLocaleString();
        this.costAmountOutput.textContent = costAmount.toLocaleString();
        this.grossProfitOutput.textContent = grossProfit.amount;
        this.grossProfitRateOutput.textContent = grossProfit.rate;
    }
}
