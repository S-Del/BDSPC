export type GetMarketSalesCommand = {
    readonly itemName: string;
    readonly unitPrice: number;
    readonly itemQuantity: number;
};
