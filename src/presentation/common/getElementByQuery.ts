export const getElementByQuery = <T extends HTMLElement>(
    query: string,
    parent?: HTMLElement
): T => {
    const target = (() => {
        if (!parent) {
            return document.body.querySelector<T>(query);
        }

        return parent.querySelector<T>(query);
    })();

    if (!target) {
        throw new Error(`指定された要素 ${query} が見つからなかった`);
    }

    return target;
}
