import { getElementByQuery } from './presentation/common';
import {
    AddDelivelyItemEvent,
    AddItemEvent,
    UpdateResultEvent
} from './presentation/event';

const marketSalesTBody = getElementByQuery<HTMLTableSectionElement>(
    '.marketSalesTBody'
);
new AddItemEvent(
    getElementByQuery<HTMLFormElement>('form[name="marketSalesForm"]'),
    marketSalesTBody
);

const delivelyItemTbody = getElementByQuery<HTMLTableSectionElement>(
    '.delivelyItemTBody'
);
new AddDelivelyItemEvent(
    getElementByQuery<HTMLFormElement>('form[name="delivelyItemForm"]'),
    delivelyItemTbody
);

const costItemTBody = getElementByQuery<HTMLTableSectionElement>(
    '.costItemTBody'
);
new AddItemEvent(
    getElementByQuery<HTMLFormElement>('form[name="costInputForm"]'),
    costItemTBody
);

new UpdateResultEvent(
    getElementByQuery<HTMLTableElement>('table[class="resultTable"]'),
    getElementByQuery<HTMLInputElement>('input[name="isPremium"]'),
    marketSalesTBody,
    delivelyItemTbody,
    costItemTBody
);
