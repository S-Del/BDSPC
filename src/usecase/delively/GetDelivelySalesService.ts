import { GetDelivelySalesCommand } from '.';
import { Persentage } from '../../domain/model';
import { DelivelyPrice } from '../../domain/model/delively';
import { ItemQuantity } from '../../domain/model/item';
import { Silver } from '../../domain/model/sales';

export class GetDelivelySalesService {
    readonly handle = (...commands: GetDelivelySalesCommand[]): number => {
        return commands.reduce((prev, command): number => {
            return prev + new DelivelyPrice(
                new Silver(command.unitPrice),
                new Persentage(command.giftBonus),
                new Persentage(command.skillBonus),
                new ItemQuantity(command.quantity)
            ).total().value;
        }, 0);
    }
}
