export type GetCostAmountCommand = {
    readonly itemName: string;
    readonly unitPrice: number;
    readonly itemQuantity: number;
};
