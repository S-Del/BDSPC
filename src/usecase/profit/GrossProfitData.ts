export type GrossProfitData = {
    readonly amount: string;
    readonly rate: string;
}
