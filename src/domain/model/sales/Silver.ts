import { IValueObject } from '../IValueObject';

export class Silver implements IValueObject {
    static readonly UNIT = 'シルバー';

    readonly value: number;

    constructor(value: number) {
        this.value = value;
    }

    readonly equals = (other: unknown): boolean => {
        return other instanceof Silver
               && other.value === this.value;
    }

    readonly toString = (): string => {
        return `${this.value.toLocaleString()} ${Silver.UNIT}`;
    }
}
