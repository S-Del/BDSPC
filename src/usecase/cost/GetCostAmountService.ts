import { GetCostAmountCommand } from '.';
import { ItemPrice, ItemPriceSet, ItemQuantity } from '../../domain/model/item';
import { Silver } from '../../domain/model/sales';

export class GetCostAmountService {
    readonly handle = (...commands: GetCostAmountCommand[]): number => {
        return new ItemPriceSet(
            ...commands.map((command: GetCostAmountCommand): ItemPrice => {
                return new ItemPrice(
                    command.itemName,
                    new Silver(command.unitPrice),
                    new ItemQuantity(command.itemQuantity)
                );
            })
        ).total().value;
    }
}
