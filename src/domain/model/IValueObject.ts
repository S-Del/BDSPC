export interface IValueObject {
    equals(other: unknown): boolean;
}
