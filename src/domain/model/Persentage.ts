import { IValueObject } from './IValueObject';

export class Persentage implements IValueObject {
    static readonly DIVISOR = 100;
    static readonly UNIT = '%';

    readonly value: number;

    constructor(...values: number[]) {
        this.value = values.reduce((prev, crnt): number => prev + crnt, 0);
    }

    readonly equals = (other: unknown): boolean => {
        return other instanceof Persentage
               && other.value === this.value;
    }

    readonly multiplier = (): number => this.value / Persentage.DIVISOR;

    readonly toString = (digit=2): string => {
        digit = 10**digit;
        return `${Math.round(this.value * digit) / digit} ${Persentage.UNIT}`;
    }
}
