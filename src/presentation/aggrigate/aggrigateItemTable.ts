import { getElementByQuery } from '../common';

export const aggrigateItemTable = (itemTableBody: HTMLTableSectionElement) => {
    const items = Array.from(itemTableBody.rows);

    return items.map((row: HTMLTableRowElement) => {
        const itemName = getElementByQuery<HTMLTableCellElement>(
            'td[class="nameCell"]', row
        ).dataset.itemName;
        if (!itemName) {
            throw new Error('dataset にアイテム名が保存されていない');
        }

        const unitPrice = getElementByQuery<HTMLTableCellElement>(
            'td[class="priceCell"]', row
        ).dataset.unitPrice;
        if (!unitPrice) {
            throw new Error('dataset にアイテム価格が保存されていない');
        }

        const itemQuantity = getElementByQuery<HTMLTableCellElement>(
            'td[class="quantityCell"]', row
        ).dataset.itemQuantity;
        if (!itemQuantity) {
            throw new Error('dataset にアイテム数量が保存されていない');
        }

        return {
            itemName,
            unitPrice: Number(unitPrice),
            itemQuantity: Number(itemQuantity)
        };
    });
}
