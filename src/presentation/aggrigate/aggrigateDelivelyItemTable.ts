import { GetDelivelySalesCommand } from '../../usecase/delively';
import { getElementByQuery } from '../common';

export const aggrigateDelivelyItemTable = (
    delivelyItemTableBody: HTMLTableSectionElement
): GetDelivelySalesCommand[] => {
    const items = Array.from(delivelyItemTableBody.rows);

    return items.map((row: HTMLTableRowElement): GetDelivelySalesCommand => {
        const unitPrice = getElementByQuery<HTMLTableCellElement>(
            'td[class="unitPriceCell"]', row
        ).dataset.unitPrice;
        if (!unitPrice) {
            throw new Error('dataset にアイテム単価が保存されていない');
        }

        const giftBonus = getElementByQuery<HTMLTableCellElement>(
            'td[class="giftBonusCell"]', row
        ).dataset.giftBonus;
        if (!giftBonus) {
            throw new Error('dataset に納品時追加額が保存されていない');
        }

        const skillBonus = getElementByQuery<HTMLTableCellElement>(
            'td[class="skillBonusCell"]', row
        ).dataset.skillBonus;
        if (!skillBonus) {
            throw new Error('dataset に熟練度追加額が保存されていない');
        }

//      const distanceBonus = ...

//      const negotiationBonus = ...

        const quantity = getElementByQuery<HTMLTableCellElement>(
            'td[class="quantityCell"]', row
        ).dataset.quantity;
        if (!skillBonus) {
            throw new Error('dataset にアイテム数量が保存されていない');
        }

        return {
            unitPrice: Number(unitPrice),
            giftBonus: Number(giftBonus),
            skillBonus: Number(skillBonus),
//          distanceBonus: Number(distanceBonus),
//          negotiationBonus: Number(negotiationBonus),
            quantity: Number(quantity),
        }
    });
}
