import { GetTaxIncludedSalesCommand } from '.';
import { Silver, TaxIncludedSales } from '../../domain/model/sales';

export class GetTaxIncludedSalesService {
    readonly handle = (command: GetTaxIncludedSalesCommand): number => {
        return new TaxIncludedSales(
            new Silver(command.marketSales),
            command.isPremium
        ).profit().value;
    }
}
