export type GetDelivelySalesCommand = {
    readonly unitPrice: number;
    readonly giftBonus: number;
    readonly skillBonus: number;
//  readonly distanceBonus: number;
//  readonly negotiationBonus: number;
    readonly quantity: number;
};
