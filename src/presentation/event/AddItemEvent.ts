import { getElementByQuery } from '../common';

export class AddItemEvent {
    private readonly form: HTMLFormElement;
    private readonly nameInput: HTMLInputElement;
    private readonly priceInput: HTMLInputElement;
    private readonly quantityInput: HTMLInputElement;
    private readonly addItemButton: HTMLButtonElement;
    private readonly tbody: HTMLTableSectionElement;

    constructor(form: HTMLFormElement, tbody: HTMLTableSectionElement) {
        this.form = form;
        this.nameInput = getElementByQuery<HTMLInputElement>(
            'input[name="nameInput"]', this.form
        );
        this.priceInput = getElementByQuery<HTMLInputElement>(
            'input[name="priceInput"]', this.form
        );
        this.quantityInput = getElementByQuery<HTMLInputElement>(
            'input[name="quantityInput"]', this.form
        );
        this.addItemButton = getElementByQuery<HTMLButtonElement>(
            'button[name="addItemButton"]', this.form
        );
        this.addItemButton.addEventListener('click', this.addItemRow);
        this.tbody = tbody;
    }

    readonly addItemRow = (): void => {
        const LAST = -1;
        const newRow = this.tbody.insertRow(LAST);
        this.addItemNameCell(newRow, LAST);
        this.addPriceCell(newRow, LAST);
        this.addQuantityCell(newRow, LAST);
        this.addDeleteRowButtonCell(newRow, LAST);

        this.form.reset();
        this.nameInput.focus();
    }

    readonly addItemNameCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const nameCell = row.insertCell(position);
        nameCell.className = 'nameCell';
        const name = !this.nameInput.value ? `アイテム ${row.rowIndex}`
                                           : this.nameInput.value;
        nameCell.textContent = name;
        nameCell.dataset.itemName = name;
    }

    readonly addPriceCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const priceCell = row.insertCell(position);
        priceCell.className = 'priceCell';
        priceCell.textContent = Number(this.priceInput.value).toLocaleString();
        priceCell.dataset.unitPrice = this.priceInput.value;
    }

    readonly addQuantityCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const quantityCell = row.insertCell(position);
        quantityCell.className = 'quantityCell';
        quantityCell.textContent = Number(this.quantityInput.value)
                                   .toLocaleString();
        quantityCell.dataset.itemQuantity = this.quantityInput.value;
    }

    readonly addDeleteRowButtonCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const buttonCell = row.insertCell(position);
        buttonCell.className = 'deleteButtonCell';
        const button = document.createElement('button');
        button.textContent = '削除';
        button.addEventListener('click', (): void => {
            this.tbody.deleteRow(row.rowIndex - 1);
        });
        buttonCell.appendChild(button);
    }
}
