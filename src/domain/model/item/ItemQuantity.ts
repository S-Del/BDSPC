import { IValueObject } from '../IValueObject';

export class ItemQuantity implements IValueObject {
    static readonly MIN = 0;
    static readonly UNIT = '個';

    readonly value: number;

    constructor(value: number) {
        if (!ItemQuantity.isValid(value)) {
            throw new Error('アイテム数量の指定が不正');
        }

        this.value = value;
    }

    static readonly isValid = (value: number): boolean => {
        return value >= ItemQuantity.MIN;
    }

    readonly equals = (other: unknown): boolean => {
        return other instanceof ItemQuantity
               && this.value === other.value;
    }

    readonly toString = (): string => `${this.value} ${ItemQuantity.UNIT}`;
}
