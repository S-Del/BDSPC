import { Persentage } from '..';
import { ItemQuantity } from '../item';
import { Silver } from '../sales';

export class DelivelyPrice {
    // 原価
    private readonly unitPrice: Silver;
    // 納品時追加額
    private readonly giftBonus: Persentage;
    // 熟練度追加額
    private readonly skillBonus: Persentage;
    // 運搬距離距離追加額
//  private readonly distanceBonus: Persentage;
    // 交渉追加額 5% + (貿易レベル / 2) ?
//  private readonly negotiationBonus: Persentage;
    // 数量
    private readonly quantity: ItemQuantity;

    constructor(
        unitPrice: Silver,
        giftBonus: Persentage,
        skillBonus: Persentage,
//      distanceBonus: Persentage,
//      negotiationBonus: Persentage,
        quantity: ItemQuantity
    ) {
        this.unitPrice = unitPrice;
        this.giftBonus = giftBonus;
        this.skillBonus = skillBonus;
        this.quantity = quantity;
    }

    readonly amount = (): Silver => {
        return new Silver(
            this.unitPrice.value
            * (1 + this.giftBonus.multiplier() + this.skillBonus.multiplier())
        );
    }

    readonly total = (): Silver => {
        return new Silver(this.amount().value * this.quantity.value);
    }
}
