export type GetTaxIncludedSalesCommand = {
    readonly marketSales: number;
    readonly isPremium: boolean;
}
