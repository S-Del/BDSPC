import { IEntity } from '../IEntity';
import { Silver } from '../sales/Silver';
import { ItemQuantity } from './ItemQuantity';

export class ItemPrice implements IEntity {
    readonly itemName: string;

    readonly unitPrice: Silver;
    readonly quantity: ItemQuantity;

    constructor(itemName: string, unitPrice: Silver, quantity: ItemQuantity) {
        this.itemName = itemName;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    readonly equals = (other: unknown): boolean => {
        return other instanceof ItemPrice
               && other.itemName === this.itemName;
    }

    readonly toString = (): string => {
        return [
            this.itemName,
            `\t価格: ${this.unitPrice}`,
            `\t数量: ${this.quantity}`,
            `\t合計価格: ${this.total()}`
        ].join('\n')
    }

    readonly total = (): Silver => {
        return new Silver(this.unitPrice.value * this.quantity.value);
    }
}
