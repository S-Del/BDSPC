import { GrossProfitData, GetGrossProfitCommand } from '.';
import { GrossProfit, Silver } from '../../domain/model/sales';

export class GetGrossProfitService {
    readonly handle = (command: GetGrossProfitCommand): GrossProfitData => {
        const grossProfit = new GrossProfit(
            new Silver(command.taxIncludedSales + command.delivelySales),
            new Silver(command.costAmount)
        );
        return {
            amount: grossProfit.amount().value.toLocaleString(),
            rate: grossProfit.rate().value.toLocaleString()
        };
    }
}
