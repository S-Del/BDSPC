export type GetGrossProfitCommand = {
    readonly taxIncludedSales: number;
    readonly delivelySales: number;
    readonly costAmount: number;
}
