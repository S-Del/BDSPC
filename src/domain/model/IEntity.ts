export interface IEntity {
    equals(other: unknown): boolean;
}
