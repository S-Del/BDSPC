import { Silver } from '.';
import { Persentage } from '..';

export class TaxIncludedSales {
    static readonly MARKET_TAX = new Persentage(30);
    static readonly TERRITORIAL_TAX = new Persentage(5);
    static readonly PREMIUM_PACKAGE_RATE = new Persentage(30);

    private readonly sales: Silver;
    private readonly isPremium: boolean;

    /**
     * @param {Silver} sales 税引前利益 (取引所表示価格での売上高)
     * @param {boolean} isPremium プレミアムパッケージ有効なら true
     */
    constructor(sales: Silver, isPremium: boolean) {
        this.sales = sales;
        this.isPremium = isPremium;
    }

    /**
     * `取引所表示価格 * (1 - 物品取引所税率 + 領地税率)
     *                 * (1 + プレミアムパッケージボーナス)`
     *
     * @return { Silver } 税引後利益 (取引所実売上)
     */
    readonly profit = (): Silver => {
        const profit = this.sales.value
                       * (1 - this.totalTaxRate().multiplier());
        const premiumMultiplier = 1 + TaxIncludedSales.PREMIUM_PACKAGE_RATE
                                                      .multiplier();

        return this.isPremium
                ? new Silver(profit * premiumMultiplier)
                : new Silver(profit);
    }

    /**
     * `物品取引所税率 + 領地税率`
     *
     * @return { Persentage } 合計税率
     */
    readonly totalTaxRate = (): Persentage => {
        return new Persentage(
            TaxIncludedSales.MARKET_TAX.value,
            TaxIncludedSales.TERRITORIAL_TAX.value
        );
    }
}
