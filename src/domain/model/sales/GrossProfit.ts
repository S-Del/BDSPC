import { Silver } from '.';
import { Persentage } from '..';

export class GrossProfit {
    private readonly sales: Silver;
    private readonly cost: Silver;

    constructor(sales: Silver, cost: Silver) {
        this.sales = sales;
        this.cost = cost;
    }

    /**
     * 粗利
     *
     * `売上高 - 製造原価`
     */
    readonly amount = (): Silver => {
        return new Silver(this.sales.value - this.cost.value);
    }

    /**
     * 粗利率 (%)
     *
     * `粗利 / 売上原価 * 100`
     */
    readonly rate = (): Persentage => {
        if (!this.cost.value) {
            return new Persentage(0);
        }

        return new Persentage(
            this.amount().value / this.cost.value * Persentage.DIVISOR
        );
    }
}
