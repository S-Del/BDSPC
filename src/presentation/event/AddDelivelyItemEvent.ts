import { getElementByQuery } from '../common';

export class AddDelivelyItemEvent {
    private readonly form: HTMLFormElement;
    private readonly unitPriceInput: HTMLInputElement;
    private readonly giftBonusInput: HTMLInputElement;
    private readonly skillBonusInput: HTMLInputElement;
    private readonly distanceBonusInput: HTMLInputElement;
    private readonly negotiationBonusInput: HTMLInputElement;
    private readonly quantityInput: HTMLInputElement;
    private readonly addBoxButton: HTMLButtonElement;
    private readonly tbody: HTMLTableSectionElement;

    constructor(form: HTMLFormElement, tbody: HTMLTableSectionElement) {
        this.form = form;
        this.unitPriceInput = getElementByQuery<HTMLInputElement>(
            'input[name="unitPriceInput"]', this.form
        );
        this.giftBonusInput = getElementByQuery<HTMLInputElement>(
            'input[name="giftBonusInput"]', this.form
        );
        this.skillBonusInput = getElementByQuery<HTMLInputElement>(
            'input[name="skillBonusInput"]', this.form
        );
        this.distanceBonusInput = getElementByQuery<HTMLInputElement>(
            'input[name="distanceBonusInput"]', this.form
        );
        this.negotiationBonusInput = getElementByQuery<HTMLInputElement>(
            'input[name="negotiationBonusInput"]', this.form
        );
        this.quantityInput = getElementByQuery<HTMLInputElement>(
            'input[name="quantityInput"]', this.form
        );
        this.addBoxButton = getElementByQuery<HTMLButtonElement>(
            'button[name="addBoxButton"]', this.form
        );
        this.addBoxButton.addEventListener('click', this.addBoxRow);
        this.tbody = tbody;
    }

    readonly addBoxRow = (): void => {
        const LAST = -1;
        const newRow = this.tbody.insertRow(LAST);
        this.addUnitPriceCell(newRow, LAST);
        this.addGiftBonusCell(newRow, LAST);
        this.addSkillBonusCell(newRow, LAST);
        this.addDistanceBonusCell(newRow, LAST);
        this.addNegotiationBonusCell(newRow, LAST);
        this.addQuantityCell(newRow, LAST);
        this.addDeleteRowButtonCell(newRow, LAST);

        this.form.reset();
        this.unitPriceInput.focus();
    }

    readonly addUnitPriceCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const unitPriceCell = row.insertCell(position);
        unitPriceCell.className = 'unitPriceCell';
        unitPriceCell.textContent = Number(this.unitPriceInput.value)
                                    .toLocaleString();
        unitPriceCell.dataset.unitPrice = this.unitPriceInput.value;
    }

    readonly addGiftBonusCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const giftBonusCell = row.insertCell(position);
        giftBonusCell.className = 'giftBonusCell';
        giftBonusCell.textContent = Number(this.giftBonusInput.value)
                                    .toLocaleString() + ' %';
        giftBonusCell.dataset.giftBonus = this.giftBonusInput.value;
    }

    readonly addSkillBonusCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const skillBonusCell = row.insertCell(position);
        skillBonusCell.className = 'skillBonusCell';
        skillBonusCell.textContent = Number(this.skillBonusInput.value)
                                     .toLocaleString() + ' %';
        skillBonusCell.dataset.skillBonus = this.skillBonusInput.value;
    }

    readonly addDistanceBonusCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const distanceBonusCell = row.insertCell(position);
        distanceBonusCell.className = 'distanceBonusCell';
        distanceBonusCell.textContent = Number(this.distanceBonusInput.value)
                                        .toLocaleString() + ' %';
        distanceBonusCell.dataset.distanceBonus = this.distanceBonusInput
                                                      .value;
    }

    readonly addNegotiationBonusCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const negotiationBonusCell = row.insertCell(position);
        negotiationBonusCell.className = 'negotiationBonusCell';
        negotiationBonusCell.textContent = Number(
            this.negotiationBonusInput.value
        ).toLocaleString() + ' %';
        negotiationBonusCell.dataset.negotiationBonus
                = this.negotiationBonusInput.value;
    }

    readonly addQuantityCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const quantityCell = row.insertCell(position);
        quantityCell.className = 'quantityCell';
        quantityCell.textContent = Number(this.quantityInput.value)
                                   .toLocaleString();
        quantityCell.dataset.quantity = this.quantityInput.value;
    }

    readonly addDeleteRowButtonCell = (
        row: HTMLTableRowElement, position: number
    ): void => {
        const deleteButtonCell = row.insertCell(position);
        deleteButtonCell.className = 'deleteButtonCell';
        const button = document.createElement('button');
        button.type = 'button';
        button.textContent = '削除';
        button.addEventListener('click', () => {
            this.tbody.deleteRow(row.rowIndex - 1);
        });
        deleteButtonCell.appendChild(button);
    }
}
