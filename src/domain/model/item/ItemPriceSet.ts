import { Silver } from '../sales/Silver';
import { ItemPrice } from './ItemPrice';
import { ItemQuantity } from './ItemQuantity';

export class ItemPriceSet implements Iterable<ItemPrice> {

    // ToDo: ReadonlyArray にする
    private readonly itemPriceList: ItemPrice[];

    constructor (...items: ItemPrice[]) {
        this.itemPriceList = [];

        for (let i = 0; i < items.length; i++) {
            for (let j = i+1; j < items.length; j++) {
                if (!items[i].equals(items[j])) continue;

                items[i] = new ItemPrice(
                    items[i].itemName,
                    items[j].unitPrice,
                    new ItemQuantity(
                        items[i].quantity.value + items[j].quantity.value
                    )
                );
                items.splice(j, 1);
                j--;
            }
            this.itemPriceList.push(items[i]);
        }
    }

    readonly [Symbol.iterator] = (): Iterator<ItemPrice> => {
        return this.itemPriceList[Symbol.iterator]();
    }

    readonly toString = (): string => {
        return this.itemPriceList.map((item: ItemPrice): string => {
            return `${item}`;
        }).join('\n');
    }

    readonly total = (): Silver => {
        return new Silver(
            this.itemPriceList.reduce((prev, crnt): number => {
                return prev + crnt.total().value;
            }, 0)
        );
    }
}
